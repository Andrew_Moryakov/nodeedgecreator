﻿namespace GraphCreater
{
    public interface IState
    {
        Toggle toggle { get; set; }
        void UpdateState();
    }
}