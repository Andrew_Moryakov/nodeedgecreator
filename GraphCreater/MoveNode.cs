﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace GraphCreater
{
    public class MoveNode:IState
    { 
        private bool mousePressed=false; 
        private DateTime pressedTime;
        public Toggle toggle { get; set; }
        private Node SelectedNode;
        public MoveNode(Toggle toggle)
        {
            this.toggle = toggle; 
        }
        
        public void UpdateState()
        {

            if(Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                if(!mousePressed)
                {
                    SelectedNode =
                        toggle.maps.FirstOrDefault(
                            el =>
                                el.NodeOne != null && el.NodeOne.Collision(Mouse.GetState().X, Mouse.GetState().Y, 1, 1))
                            ?.NodeOne;
                    if(SelectedNode == null)
                    {
                        SelectedNode =
                            toggle.maps.FirstOrDefault(
                                el =>
                                    el.NodeTwo != null
                                    && el.NodeTwo.Collision(Mouse.GetState().X, Mouse.GetState().Y, 1, 1))?.NodeTwo;
                    }
                    if(SelectedNode != null)
                    {
                        mousePressed = true;
                        pressedTime = DateTime.Now;

                    }
                }
                if((DateTime.Now - pressedTime).Seconds > 1)
                {
                    if(SelectedNode != null)
                    {
                        if(toggle.currentFrame + 1 == toggle.Frames.Count)
                        {
                            toggle.currentFrame++;
                            toggle.Frames.Add(toggle.currentFrame, toggle.maps.Select(el => new Map(el)).ToList());
                        }
                        
                        SelectedNode.X1 = Mouse.GetState().X - (toggle.wh / 2);
                        SelectedNode.Y1 = Mouse.GetState().Y - (toggle.wh / 2);

                        


                    }
                }
            }

            // toggle.moveEmptySpace.UpdateState();
            if (Mouse.GetState().LeftButton == ButtonState.Released && mousePressed)//Если кнопка отпущена, но была нажата и происходило перемещение, курсор находится не на узле
            {
                if((DateTime.Now - pressedTime).Seconds > 1)
                {
                    mousePressed = false;
                    pressedTime = DateTime.Now;
                    SelectedNode = null; 

                }
                mousePressed = false;
                pressedTime = DateTime.Now;
                SelectedNode = null;
            }

        }

    }
}
