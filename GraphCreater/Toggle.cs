﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Newtonsoft.Json;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;

namespace GraphCreater
{
    public class Toggle
    {
        private Texture2D nodeTexture;
        private Texture2D nodeFreeTexture;
        public List<Map> maps;
        public Dictionary<int, List<Map>> Frames = new Dictionary<int, List<Map>>();  
        private SpriteBatch spriteBatch;
        private Texture2D textureEdge;

        public StartState startState;
        public DrawEdgeState drawEdgeState;
        public ConnectingTwoNodes connectingTwoNodes;
        public ClickEmptySpace clickEmptySpace; 
        public MoveNode moveNode;
        public MoveEmptySpace moveEmptySpace;
        public RemoveNode removeNode;
        public SelectedNode selectedNode;

        public int currentFrame = -1;
        public Boolean flagMove = false;
        public Boolean flagLeaveEmptySpace;
        // public MouseState MouseStateToggle;

        private SpriteFont font;
        private readonly Node _saveNodeButton;
        private readonly Node _openNodeButton;
        private readonly Node _preNodeButton;
        private readonly Node _nextNodeButton;
        private readonly Node _newNodeButton;

        private Toggle()
        {
            previousMouseState = Mouse.GetState();
            maps = new List<Map>();

            moveNode = new MoveNode(this);
            startState = new StartState(this);
            drawEdgeState = new DrawEdgeState(this);
            connectingTwoNodes = new ConnectingTwoNodes(this);
            clickEmptySpace = new ClickEmptySpace(this);
            moveEmptySpace = new MoveEmptySpace(this);
            removeNode = new RemoveNode(this);
            selectedNode = new SelectedNode(this);
            _newNodeButton = new Node
            {
                X1 = 100,
                Y1 = 10,
                Height = 30,
                Width = 30
            };
            _saveNodeButton = new Node
                              {
                                  X1 = 150,
                                  Y1 = 10,
                                  Height = 30,
                                  Width = 30
                              };

            _openNodeButton = new Node
                              {
                                  X1 = 200,
                                  Y1 = 10,
                                  Height = 30,
                                  Width = 30
                              };
            _preNodeButton = new Node
                             {
                                 X1 = 5,
                                 Y1 = 10,
                                 Height = 30,
                                 Width = 30
                             };
        _nextNodeButton = new Node
        {
            X1 = 50,
            Y1 = 10,
            Height = 30,
            Width = 30
        };

        }

    public int wh;
        private Texture2D saveTexture;
        private Texture2D openTexture;
        private Texture2D preTexture;
        private Texture2D nextTexture;
        private Texture2D newTexture;
        readonly SaveFileDialog savefDialog = new SaveFileDialog();
        public Toggle(Texture2D textureEdge, Texture2D texture, Texture2D freeNodeTexture, Texture2D textueSave, Texture2D textureOpen, Texture2D textuePre, Texture2D textureNext, Texture2D newNext, SpriteBatch spriteBatch, SpriteFont font, int w=30) : this()
        {
            this.textureEdge = textureEdge;
            wh = w;
            nodeTexture = texture;
            nodeFreeTexture = freeNodeTexture;
            this.spriteBatch = spriteBatch;
            this.font = font;
            saveTexture = textueSave;
            openTexture = textureOpen;
            preTexture = textuePre;
            nextTexture = textureNext;
            newTexture = newNext;
            savefDialog.Filter = "Файл GraphCreater (*.gcj)|*.gcj";
            
        }
        Helper helper = new Helper();
        public MouseState previousMouseState;
        public Edge tempEdge;
        public Node freeFlyNode ;
        public Map tempMap;
         
        public Map selectMap;

       
        public void Update()
        { 
            if (previousMouseState.RightButton == ButtonState.Pressed)
            {
                removeNode.UpdateState();
            }

            if ((previousMouseState.LeftButton == ButtonState.Pressed
                && Mouse.GetState().LeftButton == ButtonState.Released))
            {
                helper.SaveGraphToJson(_saveNodeButton, Frames);
                helper.NextButton(_nextNodeButton, this);
                helper.PreButton(_preNodeButton, this);
                helper.NewWorkSpace(_newNodeButton, this);
                var tempFrames = helper.OpenGraphToJson(_openNodeButton);
                if(tempFrames!=null)
                {
                    Frames = tempFrames;
                    maps = Frames.Last().Value;
                    currentFrame = Frames.Count - 1;
                }
                selectedNode.UpdateState();//Ищем те узлы, которые совпадают с координатами курсора


                if (Mouse.GetState().Y > 40)
                {
                    clickEmptySpace.UpdateState();
                }
                connectingTwoNodes.UpdateState();
                drawEdgeState.UpdateState();
            }
            previousMouseState = Mouse.GetState();
            if (selectMap != null && freeFlyNode != null)
            //Рисуем ребро и нам нужно менять координаты второго узла ребра в соответсвии с координатами мыши
            {
                freeFlyNode.X1 = Mouse.GetState().X - (wh / 2);
                freeFlyNode.Y1 = Mouse.GetState().Y - (wh / 2);
                freeFlyNode.Height = wh;
                freeFlyNode.Width = wh;
            }



            moveEmptySpace.UpdateState();
            moveNode.UpdateState();
        }

       
        public void Draw()
        {
            var n = currentFrame + 1;
             spriteBatch.DrawString(font, n + "/" + Frames.Count, new Vector2(400, 480), Color.GhostWhite);
            _saveNodeButton.Draw(spriteBatch, saveTexture);
            _openNodeButton.Draw(spriteBatch, openTexture);
            _preNodeButton.Draw(spriteBatch, preTexture);
            _nextNodeButton.Draw(spriteBatch, nextTexture);
            _newNodeButton.Draw(spriteBatch, newTexture);


            //var tempMaps = currentFrame == Frames.Count - 1 ? maps : Frames[currentFrame];
            var tempMaps = Frames.Any()?Frames[currentFrame]:null;
            if (tempMaps != null && tempMaps.Any())
            {
                spriteBatch.DrawString(font, "Connection: " + +tempMaps.Count, new Vector2(250, 10), Color.GhostWhite);
                foreach (Map map1 in tempMaps.Where(el => el != null /*&& el.Visual*/))
                {
                    if(map1.NodeOne != null && map1.NodeTwo != null /* && map1.Visual*/)
                    {
                        new Edge(map1).Draw(spriteBatch, textureEdge);
                        map1.NodeOne.Draw(spriteBatch, nodeTexture);
                        map1.NodeTwo.Draw(spriteBatch, nodeTexture);
                    }
                    else
                        if(map1.NodeOne != null && map1.NodeTwo == null)
                        {
                            map1.NodeOne.Draw(spriteBatch, nodeTexture);
                        }

                }


                if (tempEdge != null)
                {
                    freeFlyNode.Draw(spriteBatch, nodeFreeTexture);
                    tempEdge.Point2 = new Node {X1 = Mouse.GetState().X, Y1 = Mouse.GetState().Y};
                    tempEdge.Draw(spriteBatch, textureEdge);
                }
                
                //if (maps.Any())
                //{
                //    String jsn = JsonConvert.SerializeObject(Frames);
                //}
            }

    }
    }
}
