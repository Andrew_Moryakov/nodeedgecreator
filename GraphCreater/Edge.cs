﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GraphCreater
{
    public class Edge
    {
        public Node Point1 { get; set; }
        public Node Point2 { get; set; }

        public int X1 { get; set; }
        public int X2 { get; set; }
        public int Y1 { get; set; }
        public int Y2 { get; set; }
        public Edge()
        {
            
        }
        
        public Edge(Map map)
        {
            Point1 = map.NodeOne;
            Point2 = map.NodeTwo;
        }

        public Edge(int x1, int y1, int x2, int y2)
        {
            X1 = x1;
            X2 = x2;
            Y1 = y2;
            Y2 = y2;
        }

        public void Draw(SpriteBatch spriteBatch, Texture2D texture)
        {
            if (Point1 != null)
            {
                var point1 = new Vector2(Point1.XCenter, Point1.YCenter);
                var point2 = new Vector2(Point2.XCenter, Point2.YCenter);
                texture.SetData(new[] {Color.White});

                float angle = (float) Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
                float length = Vector2.Distance(point1, point2);

                spriteBatch.Draw(texture, point1, null, Color.White, angle, Vector2.Zero, new Vector2(length, 4),
                    SpriteEffects.None, 0);
            }
        }

        public void UpdateGraph(Texture2D node, Rectangle rectangle)
        {
            throw new NotImplementedException();
        }
    }
}