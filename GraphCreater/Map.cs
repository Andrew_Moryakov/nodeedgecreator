﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace GraphCreater
{
    public class Point1
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
 
    public class Map
    {
        public Map()
        {
            
        }
        public Map(Map map)
        {
            NodeOne = Helper.CloneObject(map.NodeOne);
            NodeTwo = Helper.CloneObject(map.NodeTwo);
            Visual = map.Visual;
            Frame = map.Frame;
            Frame++;
        }
        [JsonIgnore]
        public int Frame { get; set; } = 0;
        public Node NodeOne { get; set; }
        public Node NodeTwo { get; set; }
        [JsonIgnore]
        public bool Visual { get; set; } = true;
        [JsonIgnore]
        public Node SelectedNode { get; set; }
        //public Map()
        //{
        //    Nodes = new List<Map>();
        //    point1 = new Point1();
        //    point2 = new Point2();
        //}
    }
}
