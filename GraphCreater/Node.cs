﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Newtonsoft.Json;

namespace GraphCreater
{
    public class Node
    {
        public int X1 { get; set; }
        public int Y1 { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        [JsonIgnore]
        public int Frame { get; set; } = 0;
        public int XCenter => X1 + (Width/2);
        public int YCenter => Y1 + (Height/2);

        public Node() {}

        public Node(Node node)
        {
            X1 = node.X1;
            Y1 = node.Y1;
            Height = node.Height;
            Width = node.Width;
        }

        public void Draw(SpriteBatch spriteBatch, Texture2D texture)
        {
            spriteBatch.Draw(texture, new Rectangle(X1, Y1, Width, Height), Color.White);
        }

        public void UpdateGraph(Texture2D node, Rectangle rectangle)
        {
            throw new NotImplementedException();
        }

        public bool Collision(int x1, int y1, int width, int height)
        {
            bool reslt =  new Rectangle(X1, Y1, Width, Height).Intersects(new Rectangle(x1, y1, width, height));
            return reslt;
        }
    }
}