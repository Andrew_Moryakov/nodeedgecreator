﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ButtonState = Microsoft.Xna.Framework.Input.ButtonState;

namespace GraphCreater
{
    public class Game1 : Game
    {
        MouseState previousMouseState;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch; 
        private Toggle toggle;
        
        public Game1():base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferHeight = 500;
            graphics.PreferredBackBufferWidth = 500;
        }


        protected override void Initialize()
        {
            base.Initialize();
            previousMouseState = Mouse.GetState(); 
        }

        protected override void LoadContent()
        { //  hero = Texture2D.FromStream(GraphicsDevice, TitleContainer.OpenStream(@"Images/Circle.png"));
            this.IsMouseVisible = true;
            spriteBatch = new SpriteBatch(GraphicsDevice);

            toggle = new Toggle(
                new Texture2D(GraphicsDevice, 1, 1, false, SurfaceFormat.Color),
                Texture2D.FromStream(GraphicsDevice, TitleContainer.OpenStream(@"Images/Circle.png")),
                Texture2D.FromStream(GraphicsDevice, TitleContainer.OpenStream(@"Images/Circle.png")),
                Texture2D.FromStream(GraphicsDevice, TitleContainer.OpenStream(@"Images/Save.png")),
                Texture2D.FromStream(GraphicsDevice, TitleContainer.OpenStream(@"Images/Open.png")),
                Texture2D.FromStream(GraphicsDevice, TitleContainer.OpenStream(@"Images/pre.png")),
                Texture2D.FromStream(GraphicsDevice, TitleContainer.OpenStream(@"Images/next.png")),
                 Texture2D.FromStream(GraphicsDevice, TitleContainer.OpenStream(@"Images/new.png")),
                spriteBatch, Content.Load<SpriteFont>("Font"));
           
        }

        #region Unload
        protected override void UnloadContent()
        {
        }
        #endregion

        protected override void Update(GameTime gameTime)
        {
            if(Mouse.GetState().X< GraphicsDevice.PresentationParameters.Bounds.Width && Mouse.GetState().X>0 && Mouse.GetState().Y< GraphicsDevice.PresentationParameters.Bounds.Height && Mouse.GetState().Y>0)
            toggle.Update();
            base.Update(gameTime);
        }
  

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Orange);
            spriteBatch.Begin();
            toggle.Draw();
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
