﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace GraphCreater
{
    public class DrawEdgeState:IState
    {
        public Toggle toggle { get; set; }
        public DrawEdgeState(Toggle toggle)
        {
            this.toggle = toggle;
        }
        public void UpdateState()
        {
            if (toggle.selectMap != null && toggle.freeFlyNode == null) //Кликнули туда, где есть узел, но до этого не рисовали ребро
            {
                // selectMap.Node = new Node {Frame = selectMap.Node.Frame, X1 = selectMap.Node.X1 - 3, Y1 = selectMap.Node.Y1 - 3, Width = selectMap.Node.Width += 6, Height = selectMap.Node.Height += 6};
                toggle.tempMap = toggle.selectMap; //Карта, где находятся узлы. Делаем резервную кпию, чтобы знать, что мы ресуем ребро и одна вершина уже поставлена

                toggle.freeFlyNode = new Node { X1 = Mouse.GetState().X - (toggle.wh / 2), Y1 = Mouse.GetState().Y - (toggle.wh / 2), Height = toggle.wh, Width = toggle.wh };
                //Временный второй узел, который может стать настоящим

                toggle.tempEdge = new Edge //Создаем ребро
                {
                    Point1 = toggle.selectMap.SelectedNode, //Первая точка - узел по которому кликнули
                    Point2 = toggle.freeFlyNode
                    //вторая точка - узел который можно создать в любом месте и который отображается и перемещается с вторым концом ребра
                };
            }
        }
    }
}
