﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphCreater
{
    public class ConnectingTwoNodes:IState
    {
        public Toggle toggle { get; set; }

        public ConnectingTwoNodes(Toggle toggle)
        {
            this.toggle = toggle;
        }
        public void UpdateState()
        {
            if(toggle.freeFlyNode != null) //Рисуем ребро и соединяем ребро с существующей вершиной
            {
                if(toggle.tempMap.NodeTwo == null && toggle.selectMap != null)
                    //Если соединяем два узла, а не создаем новый второй
                {
                    if(toggle.selectMap.NodeTwo == null)
                    {
                        toggle.maps.Add(new Map
                                        {
                                            NodeOne = toggle.tempMap.NodeOne,
                                            NodeTwo = toggle.selectMap.SelectedNode
                                        });
                        // toggle.tempMap.NodeTwo = toggle.selectMap.SelectedNode; 
                        // if (toggle.selectMap.NodeTwo == null)
                        if(toggle.selectMap.NodeTwo==null)
                        {
                            toggle.maps.Remove(toggle.selectMap);
                        }
                        toggle.maps.Remove(toggle.tempMap);
                    }
                    else
                    {
                        toggle.maps.Add(new Map
                        {
                            NodeOne = toggle.tempMap.NodeOne,
                            NodeTwo = toggle.selectMap.SelectedNode
                        });
                        if(toggle.tempMap.NodeTwo == null)
                        {
                            toggle.maps.Remove(toggle.tempMap);
                        }
                       
                    } 
                    //toggle.selectMap.NodeTwo=toggle.freeFlyNode;
                    //Связываем узел от которого рисуется ребро с узлом по которому кликнули
                    //toggle.Frames.Remove(toggle.Frames.Count - 1);
                    //toggle.Frames.Add(toggle.currentFrame, toggle.maps);
                }
                else
                    if(toggle.selectMap != null)
                    {
                        toggle.maps.Add(new Map
                                        {
                                            NodeOne = toggle.tempMap.SelectedNode,
                                            NodeTwo = toggle.selectMap.SelectedNode
                                        });

                        if(toggle.selectMap.NodeTwo == null)
                        {
                            toggle.maps.Remove(toggle.selectMap);
                        }
                        //toggle.Frames.Add(toggle.currentFrame, toggle.maps);
                    }
                    else
                    {
                        toggle.maps.Add(new Map
                                        {
                                            NodeOne = toggle.tempMap.SelectedNode,
                                            NodeTwo = toggle.freeFlyNode
                                        });
                    

                    if (toggle.tempMap.NodeTwo == null && toggle.tempMap.NodeOne != null)
                    {
                        toggle.maps.Remove(toggle.tempMap);
                    }
                    //toggle.Frames.Remove(toggle.Frames.Count - 1);
                    //toggle.Frames.Add(toggle.currentFrame, toggle.maps);
                }
                 
                toggle.currentFrame++;
                toggle.Frames.Add(toggle.currentFrame, toggle.maps.Select(el => new Map(el)).ToList());
                toggle.tempEdge = null;
                toggle.freeFlyNode = null;
            }
        }
    }
}
