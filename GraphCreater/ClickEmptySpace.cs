﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace GraphCreater
{
    public class ClickEmptySpace:IState
    {
        public Toggle toggle { get; set; }
        public IState state { get; set; }
        public ClickEmptySpace(Toggle toggle)
        {
            this.toggle = toggle;
        }

        public void UpdateState()
        {
            if (toggle.selectMap == null && toggle.freeFlyNode == null) //Кликнули по пустому месту, где нет узла, и мы не рисуем ребро (ни один узел не пересекся с курсором)
            {

                
                //if (toggle.currentFrame == -1)
                //{
                if (toggle.currentFrame == toggle.Frames.Count - 1)
                {
                    //if (toggle.currentFrame>-1)
                    //{
                    //    toggle.maps = toggle.Frames[toggle.currentFrame];
                    //}
                    toggle.maps.Add(new Map //Добавляем один узел
                                    {
                                        NodeOne = new Node
                                                  {
                                                      X1 = Mouse.GetState().X - (toggle.wh / 2),
                                                      Y1 = Mouse.GetState().Y - (toggle.wh / 2),
                                                      Width = toggle.wh,
                                                      Height = toggle.wh
                                                  } //Центр узла совпадает с координатами курсора, задаем размер
                                    });

                    var tempmap = toggle.maps;
                    toggle.currentFrame++;
                    toggle.Frames.Add(toggle.currentFrame, tempmap.Select(el => new Map(el)).ToList());
                }
                //else
                //{
                //    toggle.Frames[toggle.currentFrame].Add(new Map //Добавляем один узел
                //    {
                //        NodeOne = new Node
                //        {
                //            X1 = Mouse.GetState().X - (toggle.wh / 2),
                //            Y1 = Mouse.GetState().Y - (toggle.wh / 2),
                //            Width = toggle.wh,
                //            Height = toggle.wh
                //        }
                //        //Центр узла совпадает с координатами курсора, задаем размер
                //    });
                //    toggle.maps = toggle.Frames[toggle.currentFrame];
                //}
                ////}

            }
        }
    }
}
