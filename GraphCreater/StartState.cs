﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace GraphCreater
{
    public class StartState : IState
    {
        public StartState(Toggle toggle)
        {
            this.toggle = toggle;

        }

        public Toggle toggle { get; set; }

        public void UpdateState()
        {
           
        }
    }
}
