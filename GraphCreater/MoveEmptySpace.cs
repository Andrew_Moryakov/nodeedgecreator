﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace GraphCreater
{
    public class MoveEmptySpace:IState
    {
        public Toggle toggle { get; set; }

        public MoveEmptySpace(Toggle toggle)
        {
            this.toggle = toggle;
        }

        public void UpdateState()
        {
            toggle.flagLeaveEmptySpace = toggle.maps.FirstOrDefault(el => el.NodeOne!=null && el.NodeOne.Collision(Mouse.GetState().X, Mouse.GetState().Y, 1, 1)) == null;
        }
    }
}
