﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace GraphCreater
{
    public class RemoveNode : IState
    {
        public Toggle toggle { get; set; }

        public RemoveNode(Toggle toggle)
        {
            this.toggle = toggle;
        }

        public void UpdateState()
        {
            toggle.tempEdge = null;
            toggle.freeFlyNode = null;
            toggle.selectMap = null;

            var nods = toggle.maps.Where(el => el.NodeOne != null && el.NodeOne.Collision(Mouse.GetState().X, Mouse.GetState().Y, 1, 1)).ToList();
            var nodesTwo = toggle.maps.Where(el => el.NodeTwo != null && el.NodeTwo.Collision(Mouse.GetState().X, Mouse.GetState().Y, 1, 1)).ToList();

            foreach (var map in nodesTwo)
            {
                if(
                    toggle.maps.Any(
                        el =>
                            el.NodeOne.Collision(map.NodeOne.X1, map.NodeOne.Y1, map.NodeOne.Width, map.NodeOne.Height)
                            && el.NodeTwo != map.NodeTwo))
                {
                    toggle.maps.Remove(map);
                }
                else
                {
                    map.NodeTwo = null;
                }
                toggle.currentFrame++;
                toggle.Frames.Add(toggle.currentFrame, toggle.maps.Select(el => new Map(el)).ToList());
            }
           // toggle.Frames.Add(toggle.currentFrame++, toggle.maps);

            foreach (var map in nods)
            {
                if (map.NodeOne == null)
                {
                    map.NodeOne = null;
                    map.NodeTwo = null;
                }
                else
                {
                    map.NodeOne = map.NodeTwo;
                    map.NodeTwo = null;
                }
                toggle.currentFrame++;
                toggle.Frames.Add(toggle.currentFrame, toggle.maps.Select(el => new Map(el)).ToList());
            }

            var forremove = toggle.maps.Where(el => el.NodeOne == null && el.NodeTwo == null).ToList();
            if(forremove.Any() && toggle.currentFrame+1==toggle.Frames.Count)
            {
                forremove.ForEach(el => toggle.maps.Remove(el));
                toggle.currentFrame++;
                toggle.Frames.Add(toggle.currentFrame, toggle.maps.Select(el => new Map(el)).ToList());

            }
          

            //toggle.Frames.Add(toggle.currentFrame++, toggle.maps);


            //toggle.maps = tempMap;



        }
    }
}
