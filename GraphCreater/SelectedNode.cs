﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace GraphCreater
{
    public class SelectedNode:IState
    {
        public Toggle toggle { get; set; }

        public SelectedNode(Toggle toggle)
        {
            this.toggle = toggle;
        }

        public void UpdateState()
        {
            toggle.selectMap = toggle?.maps?.FirstOrDefault(el => el.NodeOne != null && el.NodeOne.Collision(Mouse.GetState().X, Mouse.GetState().Y, 1, 1));
            if (toggle?.selectMap != null)
            {
                toggle.selectMap.SelectedNode = toggle.selectMap?.NodeOne;
            }


            if (toggle?.selectMap == null)
            {
                toggle.selectMap = toggle?.maps?.FirstOrDefault(el => el.NodeTwo != null && el.NodeTwo.Collision(Mouse.GetState().X, Mouse.GetState().Y, 1, 1));
                if (toggle.selectMap != null)
                {
                    toggle.selectMap.SelectedNode = toggle?.selectMap?.NodeTwo;
                }
            }

        }
    }
}
